# Stage 0: build scripts
FROM golang:1.16
WORKDIR /usr/local/iac-cern-vm-tenant-mm-notification
COPY extra/iac-cern-vm-tenant-mm-notification ./
RUN go build

# Stage 1: build image
FROM gitlab-registry.cern.ch/ai-config-team/ai-tools:latest

LABEL maintainer "Batch Operations <batch-3rd@cern.ch>"

#
#
#
ENV TF_PLUGIN_PATH="/root/.local/share/terraform/plugins"
RUN mkdir -p $TF_PLUGIN_PATH
RUN yum install -y krb5-devel unzip jq wget rubygem-yaml-lint bc diffutils && yum clean all

# Stage 0 builds:
COPY --from=0 /usr/local/iac-cern-vm-tenant-mm-notification/batch-mm-notificator /usr/local/bin/


#
# Terraform Release
#
ENV TERRAFORM_RELEASE="1.1.6"

RUN curl -sL https://releases.hashicorp.com/terraform/${TERRAFORM_RELEASE}/terraform_${TERRAFORM_RELEASE}_linux_amd64.zip -o terraform.zip && unzip terraform.zip -d /bin && rm terraform.zip                                                                 

#
# Plugins
#

# Batch Factory
ENV TF_PLUGIN_VERSION_BATCHFACTORY="1.0.0"
ENV TF_PLUGIN_PATH_BATCHFACTORY="${TF_PLUGIN_PATH}/gitlab.cern.ch/batch-team/batchfactory/${TF_PLUGIN_VERSION_BATCHFACTORY}/linux_amd64"
RUN mkdir -p $TF_PLUGIN_PATH_BATCHFACTORY && \
    curl -sL "https://terraform-providers.s3.cern.ch/terraform-provider-batchfactory_${TF_PLUGIN_VERSION_BATCHFACTORY}.zip" -o terraform-provider-batchfactory.zip && \
    unzip terraform-provider-batchfactory.zip -d $TF_PLUGIN_PATH_BATCHFACTORY && \
    rm terraform-provider-batchfactory.zip

# CERN
ENV TF_PLUGIN_VERSION_CERN="2.0.0"
ENV TF_PLUGIN_PATH_CERN="${TF_PLUGIN_PATH}/gitlab.cern.ch/batch-team/cern/${TF_PLUGIN_VERSION_CERN}/linux_amd64"
RUN mkdir -p $TF_PLUGIN_PATH_CERN && \
    curl -sL "https://terraform-providers.s3.cern.ch/terraform-provider-cern_${TF_PLUGIN_VERSION_CERN}.zip" -o terraform-provider-cern.zip && \
    unzip terraform-provider-cern.zip -d $TF_PLUGIN_PATH_CERN && \
    rm terraform-provider-cern.zip

#
# Other dependencies
#

# Perl modules used by `validate-project-metadata` script
RUN yum install -y perl-JSON perl-Data-Compare

# Azure 'az' CLI
RUN echo -e "[azure-cli]\nname=Azure CLI\nbaseurl=https://packages.microsoft.com/yumrepos/azure-cli\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" | tee /etc/yum.repos.d/azure-cli.repo
RUN yum install -y azure-cli && yum clean all

CMD []
