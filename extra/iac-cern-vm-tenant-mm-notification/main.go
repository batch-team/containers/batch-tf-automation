package main

import (
	"fmt"
	"log"
	"os"

	"github.com/kelseyhightower/envconfig"
	"github.com/mattermost/mattermost-server/model"
)

// NotificatorConfig contains what is neede to talk to MatterMost
type NotificatorConfig struct {
	Endpoint      string
	Token         string
	ChannelName   string
	TeamName      string
	PipelineURL   string
	ProjectName   string
	AddCount      int
	DestroyCount  int
	RecreateCount int
	client        *model.Client4
	team          *model.Team
	channel       *model.Channel
}

func (mm *NotificatorConfig) initClient() error {
	if mm.client == nil {
		mm.client = model.NewAPIv4Client(mm.Endpoint)
		mm.client.AuthToken = mm.Token
		mm.client.AuthType = model.HEADER_BEARER
		if props, resp := mm.client.GetOldClientConfig(""); resp.Error != nil {
			println("There was a problem pinging the Mattermost server.  Are you sure it's running?")
			println(resp.Error.Message)
			os.Exit(1)
		} else {
			println("Server detected and is running version " + props["Version"])
		}

		t, resp := mm.client.GetTeamByName(mm.TeamName, "")
		if resp.Error != nil {
			return fmt.Errorf("we failed to get our team %s (%v)", mm.TeamName, resp.Error.Message)
		}
		mm.team = t

		c, resp := mm.client.GetChannelByName(mm.ChannelName, mm.team.Id, "")
		if resp.Error != nil {
			return fmt.Errorf("we failed to get our channel %s (%v)", mm.ChannelName, resp.Error.Message)
		}
		mm.channel = c
	}
	return nil
}

func actionSeverityIcon(add int, destroy int, recreate int) string {
	if destroy > 0 {
		return ":stop:"
	}

	if destroy == 0 && recreate == 0 {
		return ":green_heart:"
	}

	return ":warning:"
}

// PostOccurrence writes a message in Mattermost with the available actions
func (mm *NotificatorConfig) postNotification() error {
	if err := mm.initClient(); err != nil {
		return fmt.Errorf("postNotification: failed to init client (%s)", err)
	}

	post := &model.Post{
		Message: fmt.Sprintf("**%s** %s: `[ Add: %d ]` | `[ Destroy: %d ]` | `[ Recreate: %d ]` | [Review Pipeline](%s)",
			mm.ProjectName, actionSeverityIcon(mm.AddCount, mm.DestroyCount, mm.RecreateCount),
			mm.AddCount,
			mm.DestroyCount,
			mm.RecreateCount,
			mm.PipelineURL),
		ChannelId: mm.channel.Id,
		RootId:    "",
		Props:     map[string]interface{}{},
	}

	post, resp := mm.client.CreatePost(post)
	if resp.Error != nil {
		return fmt.Errorf("We failed to send a message to the logging channel (%v)", resp.Error.Message)
	}

	return nil
}

func main() {
	var c NotificatorConfig
	err := envconfig.Process("batchmm", &c)
	if err != nil {
		log.Fatal(err.Error())
	}
	c.initClient()
	c.postNotification()
}
