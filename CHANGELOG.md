# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

## [2.0.4] - 2024-19-03

### Changed

- Add diffutils RPM used by `compact` script

## [2.0.3] - 2023-11-08

### Changed

- Add perl modules used by `validate-project-metadata` script

## [2.0.2] - 2022-11-16

### Changed

- Force new aitools based image

## [2.0.1] - 2022-04-05

### Changed

- Force new aitools based image

## [2.0.0] - 2022-02-23

### Added

### Changed

- Upgraded Terraform to `1.1.6`.
- Upgraded CERN provider to `2.0.0`.

### Removed

## [1.0.1] - 2021-09-06

### Added

- Add Mattermost notification script used by iac-cern-vm-tenant to notify changes.

## [1.0.0] - 2021-08-24

### Added

- Azure CLI
- Added batchfactory provider
- Some dependencies used in the automated tenant provisioner

### Changed

- Upgraded Terraform to `1.0.5`.

### Removed

- Removed teigi provider, functionality is available in the "CERN" one.

## [0.2.0] - 2021-02-10

### Changed

- Upgraded `terraform-provider-cern` to `1.1.0`.

## [0.1.0] - 2021-02-05

### Added

- Terraform 0.13
- Teigi plugin 2.0.0
- CERN plugin 1.0.0
